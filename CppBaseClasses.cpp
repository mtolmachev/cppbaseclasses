// CppBaseClasses.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>

int main()
    {
        std::string str = "bla bla bla";
        std::cout << str << "\n";
        std::cout << str.length() << "\n";
        std::cout << str[0] << "\n";
        std::cout << str[str.length()-1] << "\n";
    }

